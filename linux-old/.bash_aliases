alias dir='ls --color'
alias rw='nitrogen --restore'
alias ncp='ncmpcpp'
alias nf='neofetch'
alias pf='pfetch'
alias vim='nvim'
alias v='nvim'
alias mkdir='\mkdir -pv'
    
alias please="sudo $(history -p \!\!)"
alias pls='sudo apt'
    
alias brc='nvim ~/.bashrc'
alias barc='nvim ~/.bash_aliases'
alias sbrc="source ~/.bashrc"

alias ai="sudo apt install"
alias ar="sudo apt remove"
alias ap="sudo apt purge"
alias au="sudo apt update"
alias aug="sudo apt upgrade"
alias afg="sudo apt-get dist-upgrade" 
alias ary="sudo apt autoremove -y"
alias ali='apt list --installed | grep'

# Make some possibly destructive commands more interactive.
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

# Shortcut for using the Kdiff3 tool for svn diffs.
alias svnkdiff3='svn diff --diff-cmd kdiff3'
